package org.opencv.samples.facedetect;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.objdetect.CascadeClassifier;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ViewSwitcher;
import android.graphics.drawable.AnimationDrawable;
import android.hardware.usb.UsbManager;

import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialProber;

public class FdActivity extends Activity implements CvCameraViewListener2, TextToSpeech.OnInitListener {

	private static final String TAG = "OCVSample::Activity";
	private static final Scalar FACE_RECT_COLOR = new Scalar(0, 255, 0, 255);
	public static final int JAVA_DETECTOR = 0;
	public static final int NATIVE_DETECTOR = 1;
	public static final int right = 0x1;
	public static final int left = 0x2;
	public static final int up = 0x4;
	public static final int down = 0x8;
	public static boolean oddFrame = false; 
	public static int frames=0;
	public static int hit = 0;
	public static boolean lost_face = true;

	private MenuItem mItemFace50;
	private MenuItem mItemFace40;
	private MenuItem mItemFace30;
	private MenuItem mItemFace20;
	private MenuItem mItemType;

	private Mat mRgba;
	private Mat mGray;
	private File mCascadeFile;
	private CascadeClassifier mJavaDetector;
	private DetectionBasedTracker mNativeDetector;

	private int mDetectorType = JAVA_DETECTOR;
	private String[] mDetectorName;

	private float mRelativeFaceSize = 0.2f;
	private int mAbsoluteFaceSize = 0;

	private CameraBridgeViewBase mOpenCvCameraView;

	private int result=0;

	AnimationDrawable eyesFrameAnimation; 

	TextToSpeech tts;
	
	public static ImageView faceanimView;
	
	Button direct1, direct2, direct3;
	
	ViewSwitcher viewSwitcher;
	
	Animation slide_in_left, slide_out_right;

	// Get UsbManager from Android.
	UsbManager manager;

	// Find the first available driver.
	UsbSerialDriver driver;
	
	int curr_view = 0;

	View firstView, secondView;
	
	public String dynMes = "0"; // Dynamixel Message
	
	Thread dynThread;
	
	boolean goon = true;
	
	int emotion = 0;

	private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
		@Override
		public void onManagerConnected(int status) {
			switch (status) {
			case LoaderCallbackInterface.SUCCESS: {
				Log.i(TAG, "OpenCV loaded successfully");

				// Load native library after(!) OpenCV initialization
				System.loadLibrary("detection_based_tracker");

				try {
					// load cascade file from application resources
					InputStream is = getResources().openRawResource(R.raw.lbpcascade_frontalface);
					File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
					mCascadeFile = new File(cascadeDir,
							"lbpcascade_frontalface.xml");
					FileOutputStream os = new FileOutputStream(mCascadeFile);

					byte[] buffer = new byte[4096];
					int bytesRead;
					while ((bytesRead = is.read(buffer)) != -1) {
						os.write(buffer, 0, bytesRead);
					}
					is.close();
					os.close();

					mJavaDetector = new CascadeClassifier(
							mCascadeFile.getAbsolutePath());
					if (mJavaDetector.empty()) {
						Log.e(TAG, "Failed to load cascade classifier");
						mJavaDetector = null;
					} else
						Log.i(TAG, "Loaded cascade classifier from "
								+ mCascadeFile.getAbsolutePath());

					mNativeDetector = new DetectionBasedTracker(
							mCascadeFile.getAbsolutePath(), 0);

					cascadeDir.delete();

				} catch (IOException e) {
					e.printStackTrace();
					Log.e(TAG, "Failed to load cascade. Exception thrown: " + e);
				}

				mOpenCvCameraView.enableView();
			}
			break;
			default: {
				super.onManagerConnected(status);
			}
			break;
			}
		}
	};

	public FdActivity() {
		mDetectorName = new String[2];
		mDetectorName[JAVA_DETECTOR] = "Java";
		mDetectorName[NATIVE_DETECTOR] = "Native (tracking)";

		Log.i(TAG, "Instantiated new " + this.getClass());
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.i(TAG, "called onCreate");
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		setContentView(R.layout.face_detect_surface_view);

		manager = (UsbManager) getSystemService(Context.USB_SERVICE);
		Log.d(TAG, "Usb manager: " + manager);
		if (manager == null) {
			Log.e(TAG, "No USB manager, exiting.");
		}
		Log.i(TAG, "good");
		driver = UsbSerialProber.acquire(manager);
		if (driver != null){
			try {
				driver.open();
				driver.setBaudRate(57600);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				Log.i(TAG, "Serial Device is not found...");
			}
		} else {
			Log.e(TAG, "No USB driver.");
		}
		
		//direct1 = (Button) findViewById(R.id.direction1);
		//direct2 = (Button) findViewById(R.id.direction2);
		//direct3 = (Button) findViewById(R.id.direction3);
		
		viewSwitcher = (ViewSwitcher) findViewById(R.id.viewswitcher);
		
		firstView = findViewById(R.id.view1);
		secondView = findViewById(R.id.view2);
		
		slide_in_left = AnimationUtils.loadAnimation(this, android.R.anim.slide_in_left);
		slide_out_right = AnimationUtils.loadAnimation(this, android.R.anim.slide_out_right);
		
		viewSwitcher.setInAnimation(slide_in_left);
		viewSwitcher.setOutAnimation(slide_out_right);
		
		dynThread = new Thread(new DynamixelThread());
		dynThread.start();

		mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.fd_activity_surface_view);
		mOpenCvCameraView.setCvCameraViewListener(this);

		faceanimView = (ImageView) findViewById(R.id.imageView1);		
		
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				faceanimView.destroyDrawingCache();
				faceanimView.setBackgroundResource(R.drawable.normal);
				
				eyesFrameAnimation = (AnimationDrawable) faceanimView.getBackground();
				eyesFrameAnimation.start();
			}
		});
		
		emotion = 1;
		
		tts = new TextToSpeech(this, this);
		
	}

	@Override
	public void onPause() {
		super.onPause();
		if (mOpenCvCameraView != null)
			mOpenCvCameraView.disableView();
	}

	@Override
	public void onResume() {
		super.onResume();
		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, this,
				mLoaderCallback);
	}

	public void onDestroy() {
		if(driver != null){
			try {
				driver.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (tts != null) {
			tts.stop();
			tts.shutdown();
		}

		super.onDestroy();
		mOpenCvCameraView.disableView();
	}

	public void onCameraViewStarted(int width, int height) {
		mGray = new Mat();
		mRgba = new Mat();
	}

	public void onCameraViewStopped() {
		mGray.release();
		mRgba.release();
	}

	public Mat onCameraFrame(CvCameraViewFrame inputFrame) {

		mRgba = inputFrame.rgba();
		mGray = inputFrame.gray();

		if (mAbsoluteFaceSize == 0) {
			int height = mGray.rows();
			if (Math.round(height * mRelativeFaceSize) > 0) {
				mAbsoluteFaceSize = Math.round(height * mRelativeFaceSize);
			}
			mNativeDetector.setMinFaceSize(mAbsoluteFaceSize);
		}

		MatOfRect faces = new MatOfRect();

		if (mDetectorType == JAVA_DETECTOR) {
			if (mJavaDetector != null)
				mJavaDetector.detectMultiScale(mGray, faces, 1.1, 2, 2, // TODO: objdetect.CV_HAAR_SCALE_IMAGE
						new Size(mAbsoluteFaceSize, mAbsoluteFaceSize), new Size());
		}
		else if (mDetectorType == NATIVE_DETECTOR) {
			if (mNativeDetector != null)
				mNativeDetector.detect(mGray, faces);
		}
		else {
			Log.e(TAG, "Detection method is not selected!");
		}

		Rect[] facesArray = faces.toArray();
		for (int i = 0; i < facesArray.length; i++){
			Core.rectangle(mRgba, facesArray[i].tl(), facesArray[i].br(), FACE_RECT_COLOR, 3);
		}

		// Track the biggest face

		if (facesArray.length > 0) {
			if(lost_face == true) {
				if (emotion != 2)
				{
					this.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							//Change face animation
							faceanimView.setBackgroundResource(R.drawable.busy_stressful);
							
							if (eyesFrameAnimation.isRunning())
								eyesFrameAnimation.stop();
							eyesFrameAnimation = (AnimationDrawable) faceanimView.getBackground();
							eyesFrameAnimation.start();
						}
					});
				
					emotion = 2;
				}
				
				//speakOut("Changing emotion");
				lost_face = false;
			}
					
			Log.i(TAG, "Face found");

			double faceWidth=0, maxFaceWidth=0;
			double faceHeight=0, maxFaceHeight=0;
			int maxFace=0;
			for(int i = 0; i < facesArray.length; i++){
				faceHeight = Math.abs(facesArray[i].tl().x - facesArray[i].br().x);
				faceWidth = Math.abs(facesArray[i].tl().y - facesArray[i].br().y);
				if ( faceHeight > maxFaceHeight || faceWidth > maxFaceWidth) {
					maxFace = i;
					maxFaceHeight = faceHeight;
					maxFaceWidth = faceWidth;
				}
			}

			double faceCenterX = (facesArray[maxFace].tl().x + facesArray[maxFace].br().x)/2.0;
			double faceCenterY = (facesArray[maxFace].tl().y + facesArray[maxFace].br().y)/2.0;

			String faceX, faceY;
			faceX = Integer.toString((int)faceCenterX);
			faceY = Integer.toString((int)faceCenterY);
			while(faceX.length() < 3)
				faceX = "0" + faceX;
			while(faceY.length() < 3)
				faceY = "0" + faceY;
			
			dynMes = "X" + faceX + faceY;
			
			//if (dynThread.getState() == Thread.State.NEW)
				//dynThread.start();
			
			frames = 0;
			
			if (viewSwitcher.getCurrentView() != secondView)
			{
				this.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						//viewSwitcher.showNext();
					}
				});
			}
			
		} else {
			
			if (emotion != 1)
			{
				this.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						//Change face animation
						
						faceanimView.setBackgroundResource(R.drawable.normal);
						
						if (eyesFrameAnimation.isRunning())
							eyesFrameAnimation.stop();
						eyesFrameAnimation = (AnimationDrawable) faceanimView.getBackground();
						eyesFrameAnimation.start();
					}
				});
				emotion = 1;
			}			
			dynMes = "0";
			//if (dynThread.getState() == Thread.State.NEW)
				//dynThread.start();
			
			// Stupid way to count 3 sec
			frames++;
			hit = 0;
			
			if (frames > 25){
				lost_face = true;
				
				if (viewSwitcher.getCurrentView() != firstView)
				{
					this.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							//viewSwitcher.showPrevious();
						}
					});
				}
			}
			
			if (frames > 25*3){
				//	idleAction();
				dynMes = "X180180";
				//if (dynThread.getState() == Thread.State.NEW)
					//dynThread.start();
				
				/*// sleep 2s
				Handler handler = new Handler(); 
			    handler.postDelayed(new Runnable() { 
			         public void run() { 
			              my_button.setBackgroundResource(R.drawable.defaultcard); 
			         } 
			    }, 2000);
				 */ 
				frames = 0;
				Log.i(TAG, "Idle action");
			}
			
			
		} 
		return mRgba;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		Log.i(TAG, "called onCreateOptionsMenu");
		mItemFace50 = menu.add("Face size 50%");
		mItemFace40 = menu.add("Face size 40%");
		mItemFace30 = menu.add("Face size 30%");
		mItemFace20 = menu.add("Face size 20%");
		mItemType = menu.add(mDetectorName[mDetectorType]);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Log.i(TAG, "called onOptionsItemSelected; selected item: " + item);
		if (item == mItemFace50)
			setMinFaceSize(0.5f);
		else if (item == mItemFace40)
			setMinFaceSize(0.4f);
		else if (item == mItemFace30)
			setMinFaceSize(0.3f);
		else if (item == mItemFace20)
			setMinFaceSize(0.2f);
		else if (item == mItemType) {
			int tmpDetectorType = (mDetectorType + 1) % mDetectorName.length;
			item.setTitle(mDetectorName[tmpDetectorType]);
			setDetectorType(tmpDetectorType);
		}
		return true;
	}

	private void setMinFaceSize(float faceSize) {
		mRelativeFaceSize = faceSize;
		mAbsoluteFaceSize = 0;
	}

	private void setDetectorType(int type) {
		if (mDetectorType != type) {
			mDetectorType = type;

			if (type == NATIVE_DETECTOR) {
				Log.i(TAG, "Detection Based Tracker enabled");
				mNativeDetector.start();
			} else {
				Log.i(TAG, "Cascade detector enabled");
				mNativeDetector.stop();
			}
		}
	}

	private void idleAction() {

	}

	private int sendToDynamixels(String msg){
				
			try {	
			
			if (driver != null){
				byte[] buffer = msg.getBytes();
				driver.write(buffer, buffer.length);
			} else {
				return -1;
			}

			} catch (IOException e) {
				e.printStackTrace();
			}
		
		return 0;
	}

	@Override
	public void onInit(int status) {
		// TODO Auto-generated method stub
		if (status == TextToSpeech.SUCCESS) {
			//set Language
			result = tts.setLanguage(Locale.US);
			// tts.setPitch(5); // set pitch level
			// tts.setSpeechRate(2); // set speech speed rate
			if (result == TextToSpeech.LANG_MISSING_DATA
					|| result == TextToSpeech.LANG_NOT_SUPPORTED) {
				Log.e("TTS", "No locality files");
			} 
		} else {
			Log.e("TTS", "Initilization Failed");
		}
	}

	private void speakOut(String text) {

		if(result!=tts.setLanguage(Locale.US))
		{
			Toast.makeText(getApplicationContext(), "Hi Please enter the right Words......  ", Toast.LENGTH_LONG).show();
		}else
		{
			tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
		}
	}	
	
	private class DynamixelThread implements Runnable
	{
		@Override
		public void run() {
			while (goon)
			{
				SystemClock.sleep(20);
				sendToDynamixels(dynMes);
			}
		}
	}
}
